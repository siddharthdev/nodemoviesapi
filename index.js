var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;

mongoose.connect('mongodb://127.0.0.1:27017/moviesapidb');

var Movie = require('./app/models/movie')

var router = express.Router();

router.get('/', function(req, res) {
    res.json({ message: 'hooray! welcome to our api!' });
});

router.route('/movies').post(function(req, res){
    var movie = new Movie();

    movie.title = req.body.title;
    movie.releaseDate = req.body.releaseDate;
    movie.poster = req.body.poster;
    movie.director = req.body.director;
    movie.actors = req.body.actors;
    movie.rating = req.body.rating;

    movie.save(function(err){
        if(err)
            res.send(err);
        
        res.json({messge: 'Created successfully.'});
    });
})
.get(function(req, res) {
    Movie.find({}, function(err, movies) {
        if (err)
            res.send(err);

        res.json(movies);
    });
});

router.route('/movies/:movie_id')
    
    .get(function(req, res) {
        Movie.findById(req.params.movie_id, function(err, movie) {
            if (err)
                res.send(err);
            res.json(movie);
        });
    })
    .put(function(req, res) {
        
        Movie.findById(req.params.movie_id, function(err, movie) {

            if (err)
                res.send(err);

            movie.title = req.body.title;
            movie.releaseDate = req.body.releaseDate;
            movie.poster = req.body.poster;
            movie.director = req.body.director;
            movie.actors = req.body.actors;
            movie.rating = req.body.rating;

            // save the movie
            movie.save(function(err) {
                if (err)
                    res.send(err);

                res.json({ message: 'Movie updated!' });
            });

        });
    })
    .delete(function(req, res) {
        Movie.remove({
            _id: req.params.movie_id
        }, function(err, movie) {
            if (err)
                res.send(err);

            res.json({ message: 'Successfully deleted' });
        });
    });

app.all('/*', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With,content-type");
  next();
});

app.use('/api', router);
app.listen(port);
console.log('Magic happens on port ' + port);
