var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var MovieSchema = new Schema({
  title: {type: String, required: true},
  releaseDate: Date,
  poster: String,
  director: String,
  actors: String,
  rating: Number
})


module.exports = mongoose.model('Movie', MovieSchema);
